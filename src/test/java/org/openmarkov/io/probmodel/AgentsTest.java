package org.openmarkov.io.probmodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URL;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.exception.ParserException;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.StringWithProperties;

public class AgentsTest {

    private static String rootPath;
    
	private static String probNetManualName = "test-decpomdp-manual.pgmx";
	
	private static ProbNet manualProbNet;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		URL url = getClass().getClassLoader ().getResource (probNetManualName);
		File file = new File(url.getPath());
		String absolutePath = file.getAbsolutePath();
		rootPath = absolutePath.substring(0, absolutePath.length() - probNetManualName.length());
	}

	@Test
	public void testAgentsNumber() throws ParserException {
		manualProbNet = new PGMXReader().loadProbNet(rootPath + probNetManualName).getProbNet();
		List<StringWithProperties> agents = manualProbNet.getAgents();
		assertEquals(2, agents.size());
		StringWithProperties agent1 = agents.get(0);
		assertTrue(agent1.string.contentEquals("Agent 1"));
		StringWithProperties agent2 = agents.get(1);
		assertTrue(agent2.string.contentEquals("Agent 2"));
	}


}
