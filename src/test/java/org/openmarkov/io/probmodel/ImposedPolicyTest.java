package org.openmarkov.io.probmodel;

import java.io.File;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.exception.ParserException;
import org.openmarkov.core.model.network.ProbNet;

public class ImposedPolicyTest {

	private String networkTestName = "ImposedPolicy.pgmx";
	
	private String absolutePath;
	
	private String rootPath;
	
	private PGMXWriter writer;
	
	private PGMXReader reader;
	
	@Before
    public void setUp() throws Exception {
    	URL url = getClass().getClassLoader ().getResource (networkTestName);
		File file = new File(url.getPath());
		absolutePath = file.getAbsolutePath();
		rootPath = absolutePath.substring(0, absolutePath.length() - networkTestName.length());
		writer = new PGMXWriter();
		reader = new PGMXReader();
    }
    
	@Test
	public final void test() throws ParserException {
		String rootPath = 
				absolutePath.substring(0, absolutePath.length() - networkTestName.length());
		String pathAndName = rootPath + networkTestName;
		ProbNet probNet1 = reader.loadProbNet(pathAndName).getProbNet ();

		ProbNet imposedPolicyNet = Util.createTrivialID();
	}

}
